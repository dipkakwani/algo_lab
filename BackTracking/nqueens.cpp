//Author Diptanshu Kakwani
#include<bits/stdc++.h>
using namespace std;
bool nQueen(vector<int>& chessBoard, int curRow, int n)
{
    if (curRow == n)	//Traversed all the way down to the bottom
    	return 1;

    for (int i = 0; i < n; i++)
    {
    	bool flag = true;
    	for (int j = 0; j < curRow; j++)
    		if (i == chessBoard[j] || abs(chessBoard[j] - i) == abs(j - curRow))
    			// Same column or diagonal
    			flag = false;	

    	if (flag)
    	{
    		cout << "Placing in col" << i << " in row" << curRow << endl;
    		chessBoard[curRow] = i;
    		if (nQueen(chessBoard, curRow + 1, n))
    			return 1;
    		//Restore the previous value
    		chessBoard[curRow] = -1;
    	}
    }
	return 0;	
}
int main()
{
	int n;
	cin >> n;
	vector<int> chessBoard(n, -1);
	if (nQueen(chessBoard, 0, n))
	{
		for (int row = 0; row < n; row++)
			cout << chessBoard[row] << "  ";
		cout << endl;
	}
	else
		cout << "Fail\n";
	return 0;
}
