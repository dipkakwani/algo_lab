#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vpii;
typedef vector<vpii> g;
vi bfs(g& graph, int source)
{
    queue<int> q;
    int v = graph.size();
    vi visited(v), result;
    q.push(source);
    result.push_back(source);
    visited[source] = 1;
    while (!q.empty())
    {
        int curVertex = q.front();
        q.pop();
        for (auto i = graph[curVertex].begin(); i != graph[curVertex].end(); i++)
        {
            if (!visited[(*i).first])
            {
                visited[(*i).first] = 1;
                result.push_back((*i).first);
                q.push((*i).first);
            }
        }
    }
    return result;
}
int main()
{
    int v, e, v1, v2, w;
    cin >> v >> e;
    g graph(v);
    for (int i = 0; i < e; i++)
    {
        cin >> v1 >> v2;
        graph[v1 - 1].push_back(pii((v2 - 1), 1));
    }
    vi result = bfs(graph, 0);
    cout << "The order of traversal ";
    for (auto i = result.begin(); i != result.end(); i++)
        cout << (*i) + 1 << "   ";
    cout << endl;
    return 0;
}
