#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef pair<int, int> pii;
typedef vector< pii > vpii;
typedef vector< vpii > g;
typedef tuple<int, int, int> tpl;
class disjointSets
{
    vi parent;
public:
    disjointSets(int v)
    {
        for (int i = 0; i < v; i++)
            parent.push_back(i);
    }
    int findSet(int u)
    {
        return (parent[u] == u)? u : (parent[u] = findSet(parent[u]));
    }
    bool isSameSet(int u, int v)
    {
        return (findSet(u) == findSet(v))?1:0;
    }
    void unionSet(int u, int v)
    {
        parent[findSet(u)] = findSet(v);
    }
};
void kruskal(g& graph)
{
    int v = graph.size();
    vector<tpl> edges;
    for (int i = 0; i < v; i++)
    {
        for (auto j = graph[i].begin(); j != graph[i].end(); j++)
            edges.push_back(tpl((*j).second, (*j).first, i));
    }
    sort(edges.begin(), edges.end());
    for (auto i = edges.begin(); i != edges.end(); i++)
        cout << (get<2>(*i)) << "  " << get<1>(*i) << endl;
    disjointSets vertices(v);
    int count = 0;
    for (auto i = edges.begin(); i != edges.end() && count < v; i++)
    {
        int u = get<2>(*i);
        int v = get<1>(*i);
        int w = get<0>(*i);
        if (!vertices.isSameSet(u, v))
        {
            cout << "Edge " << u << " to " << v << " with weight " << w << endl;
            vertices.unionSet(u, v);
            count++;
        }
    }
}
int main()
{
   	int v, e;
	cout << "Enter no. of vertices and edges\n";
	cin >> v >> e;
	cout << "Enter the edges (e1 e2 weight)\n";
	vector< vector< pair<int, int> > > graph(v);
	for (int i = 0; i < e; i++)
	{
		int a, b, c;
		cin >> a >> b >> c;
		graph[a - 1].push_back(pair<int, int>(b - 1, c));
		//graph[b - 1].push_back(pair<int, int>(a - 1, c));
	}
    kruskal(graph);
    return 0;
}
