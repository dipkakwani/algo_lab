#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<vpii> g;
typedef tuple<int, int, int> tpl;
void process(int v, g& graph, priority_queue<tpl>& Q, vector<bool>& taken)
{
    taken[v] = 1;
    for (auto i = graph[v].begin(); i != graph[v].end(); i++)
        if (!taken[(*i).first])
            Q.push(tpl(-(*i).second, -(*i).first, -v));
}

void prim(g& graph, int root)
{
	int v = graph.size();
    vector<bool> taken(v, false);
    priority_queue<tpl> Q;
    process(root, graph, Q, taken);
    int mst_weight = 0;
    while (!Q.empty())
    {
        tpl i = Q.top();
        Q.pop();
        int u = -get<2>(i);
        int v = -get<1>(i);
        int w = -get<0>(i);
        if (!taken[v])
        {
            cout << "Edge " << u << " to  " << v << " with weight " << w << endl;
            process(v, graph, Q, taken);
            mst_weight += w;
        }
    }
}
int main()
{
	int v, e;
	cout << "Enter no. of vertices and edges\n";
	cin >> v >> e;
	cout << "Enter the edges (e1 e2 weight)\n";
	vector< vector< pair<int, int> > > graph(v);
	for (int i = 0; i < e; i++)
	{
		int a, b, c;
		cin >> a >> b >> c;
		graph[a - 1].push_back(pair<int, int>(b - 1, c));
		//graph[b - 1].push_back(pair<int, int>(a - 1, c));
	}
   	prim(graph, 0);
	return 0;
}
