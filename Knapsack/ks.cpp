#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
typedef vector<int> vi;
vi ks_bottom_up(vi& weight, vi& value, int capacity)
{
	int n = weight.size();
	vector< vi > dp(n + 1, vi (capacity + 1));
	for (int i = 1; i < n + 1; i++)
	{
		for (int j = 1; j < capacity + 1; j++)
		{
			if (j - weight[i - 1] >= 0)
				dp[i][j] = max(dp[i - 1][j], value[i - 1] + dp[i - 1][j - weight[i - 1]]);
			else
				dp[i][j] = dp[i - 1][j];
		}
	}
	cout << "Result\n";
	for (int i = 0; i < n + 1; i++)
	{
		for (int j = 0; j < capacity + 1; j++)
			cout << dp[i][j] << " ";
		cout << endl;
	}
	//Get all the elements which were selected
	vi result;
	int i = n, j = capacity;
	while (i >= 1 && j >= 1)
	{
		if (dp[i][j] != dp[i -1][j])
		{
			result.push_back(i);
			j -= weight[i - 1];
		}
		i--;
	}
	return result;
}
int main()
{
	int n, capacity;
	cout << "Enter no. of items\n";
	cin >> n;
	vi weight(n), value(n);
	cout << "Enter weight and value of each item\n";
	for (int i = 0; i < n; i++)
		cin >> weight[i] >> value[i];
	cout << "Enter the capacity of the knapsack\n";
	cin >> capacity;
	vi res = ks_bottom_up(weight, value, capacity);
	reverse(res.begin(), res.end());
	for (vi::iterator it = res.begin(); it != res.end(); it++)
		cout << *it << " ";
	cout << endl;
	return 0;
}
