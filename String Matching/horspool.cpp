#include <bits/stdc++.h>
using namespace std;
int cnt;
long long horspool(string& pattern, string& text, vector<long long>& table)
{
	long long m = pattern.length(), n = text.length();
	long long i = m - 1;
	while (i < n)
	{
		long long k = 0;
		while (k < m && pattern[m - 1 - k] == text[i - k])
		{
			k++;
			cnt++;
		}
		cnt++;
		if (k == m)
			return i - m + 1;
		else
			i += table[text[i]];
	}
	return -1;
}

int main()
{
	string pattern, text;
	cnt = 0;
	cout << "Enter the text\n";
	getline(cin, text);
	cout << "Enter the pattern\n";
	getline(cin, pattern);
	long long m = pattern.length(), n = text.length();
	
	/*
		Compute Shift Table
	*/
	vector<long long> table(CHAR_MAX, m);
	for (long long j = 0; j < m - 1; j++)
		table[pattern[j]] = m - 1 - j;

	long long idx = horspool(pattern, text, table);
	if (idx == -1)
		cout << "Pattern not found\n";
	else
		cout << "Pattern found at " << idx + 1 << endl;
	cout << "No. of Comparisons = " << cnt << endl;
	return 0;
}
