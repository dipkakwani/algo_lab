//Author : Diptanshu Kakwani
#include<bits/stdc++.h>
using namespace std;
#define LEFT(i) (2 * i + 1)
#define RIGHT(i) (2 * i + 2)
#define PARENT(i) ((i - 1) / 2)
template<class T>
class heap
{
public:
	vector<T> v;
	size_t sz;
	heap(vector<T> a); 	//Construct heap from an array
	void max_heapify(int);
	vector<T> heap_sort();
};
template<class T>
heap<T>::heap(vector<T> a)
{
	v = a;
	sz = v.size();
	for (int i = sz / 2 - 1; i >= 0; i--)
		max_heapify(i);
}
template<class T>
void heap<T>::max_heapify(int i)
{
	size_t largest;
	size_t left = LEFT(i);
	size_t right = RIGHT(i);
	if (left < sz && v[left] > v[i])
		largest = left;
	else
		largest = i;
	if (right < sz && v[right] > v[largest])
		largest = right;
	if (largest != i)
	{
		swap(v[i], v[largest]);
		max_heapify(largest);
	}
}
template<class T>
vector<T> heap<T>::heap_sort()
{
	vector<T> result;
	for (int j = v.size() - 1; j > 0; j--)
	{
		result.push_back(v[0]);
		swap(v[0], v[j]);
		sz--;
		max_heapify(0);
	}
	result.push_back(v[0]);
	reverse(result.begin(), result.end());
	return result;
}
int main()
{
	vector<int> v = {4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
	heap<int> H(v);
	for (auto i = H.v.begin(); i != H.v.end(); i++)
		cout << *i << "  ";
	cout << endl;
	vector<int> result = H.heap_sort();
	for (auto i = result.begin(); i != result.end(); i++)
		cout << *i << "  ";
	cout << endl;
	return 0;
}